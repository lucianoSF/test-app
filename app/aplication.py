from flask import Flask, request
from flask import jsonify
import json


app = Flask(__name__)
@app.route('/api/v1/reservations', endpoint='reservations', methods=["GET"])
@app.route('/api/v1/reservations/1', endpoint='1', methods=["PUT"])
def create_app():
	#import rest
	#rest.answer(app)
	#return app
	dados = {"_id":"1","template":{"metadata":{"baremetal":[{"hostname":"luciano_host1","image":"ubuntu_16_04_image"}]}},"status":"SCHEDULED"}

	if request.endpoint == 'reservations':
		return jsonify(dados)

	elif request.endpoint == '1':
		dados["status"] = "PROVISIONING"
		return jsonify(dados)
