FROM python:3.8-slim-buster

WORKDIR /app

COPY ./app .
COPY ./app/requirements.txt requirements.txt
COPY ./test ./test

# install requirements
RUN pip3 install -r requirements.txt


# export run file
ENV FLASK_APP=aplication.py
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
